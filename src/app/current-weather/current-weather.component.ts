import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { WeatherClass } from '../weather-class';
import 'rxjs/Rx';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.css']
})
export class CurrentWeatherComponent implements OnInit {
  myWeather: WeatherClass;
  location;

  constructor(private ws: WeatherService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.data.subscribe(
      (data: { myWeather: WeatherClass }) => {
        this.myWeather = data.myWeather;
      }
    )
  }

  onSubmit(weatherForm: NgForm) {
    this.ws.anotherCityWeather(weatherForm.value.zip).subscribe(
      (data)=>{
        console.log("hi",data);
        this.myWeather = new WeatherClass(data.name,
          data.main.temp,
          data.weather[0].icon,
          data.weather[0].description,
          data.main.temp_max,
          data.main.temp_min);
          
      })

  }

}
