import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ForecastClass } from '../forecast-class';
import { WeatherService } from '../weather.service';
import 'rxjs/Rx';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  constructor(private ws: WeatherService) { }

  forecastForm: FormGroup;
  cityForecast: ForecastClass[] = [];
  
  ngOnInit() {
    this.forecastForm = new FormGroup({
      forecastCity: new FormControl('')
    })
  }

  onSubmit() {
    this.cityForecast=[];
    console.log(this.forecastForm);
    this.ws.nextDaysForecast(this.forecastForm.value.forecastCity).subscribe(
      (data) => {
        console.log(data);
        for (let i = 0; i < data.list.length; i += 8) {
          const temp = new ForecastClass(
            data.city.name,
            data.list[i].dt_txt,
            data.list[i].weather[0].icon,
            data.list[i].main.temp_max,
            data.list[i].main.temp_min);
          this.cityForecast.push(temp);
        }
        console.log(this.cityForecast)
    });
  }

}
