import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { ForecastComponent } from './forecast/forecast.component';
import {weatherRouting} from './weather.routing';
import { WeatherService } from './weather.service';
import { HttpModule } from '@angular/http';
import { ResolvePromiseService } from './resolve-promise.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CurrentWeatherComponent,
    ForecastComponent
  ],
  imports: [
    BrowserModule,
    weatherRouting,
    HttpModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [WeatherService,
    ResolvePromiseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
