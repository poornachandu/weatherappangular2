import { Injectable } from '@angular/core';
import { WeatherService } from './weather.service';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class ResolvePromiseService implements Resolve<any>{
  // resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  //   throw new Error("Method not implemented.");
  // }
  
  constructor(private ws:WeatherService) { }
  resolve(){
    return this.ws.localWeather();
  }

}
