export class WeatherClass {
    constructor(public cityName: string, public temperature: string, public icon: string, public weatherKind: string,
                public tempMax: string, public tempMin: string){}
}
