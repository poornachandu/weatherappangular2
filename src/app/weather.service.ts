import { Injectable } from '@angular/core';
import { WeatherClass } from './weather-class';
import { Http, Response } from '@angular/http';
import { ForecastClass } from './forecast-class';
import 'rxjs/Rx';

@Injectable()
export class WeatherService {
  presentWeather: WeatherClass;
  location;

  constructor(private http: Http) { }

  // weatherNow() {
  //   return this.presentWeather;
  // }

  localWeather(lat: string, lon: string) {
    return new Promise((res, rej) => {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.location = pos.coords;
        console.log(this.location);
        const lat = this.location.latitude;
        const lon = this.location.longitude;
        return this.http.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=a5447816724d07ce93b1f5320367a550&units=imperial`).map((response: Response) =>
          response.json()).toPromise().then(
          (data) => {
            console.log(data);
            this.presentWeather = new WeatherClass(data.name,
              data.main.temp,
              data.weather[0].icon,
              data.weather[0].description,
              data.main.temp_max,
              data.main.temp_min);
            res(this.presentWeather);
          }
          )
      });
    })
  }

  anotherCityWeather(zip: string) {
    let zipArray = zip.split(",");
    let responses = [];
    console.log("zipArray", zipArray);
    //if (zipArray.length == 1) {
      //responses.push(this.http.get(`http://api.openweathermap.org/data/2.5/weather?zip=${zip},us&appid=a5447816724d07ce93b1f5320367a550&units=imperial`));
      return this.http.get(`http://api.openweathermap.org/data/2.5/weather?zip=${zip},us&appid=a5447816724d07ce93b1f5320367a550&units=imperial`).
        map((response: Response) => response.json());
    // } else {
    //   for (let i = 0; i < zipArray.length; i++) {
    //     responses.push(this.http.get(`http://api.openweathermap.org/data/2.5/weather?zip=${zipArray[0]},us&appid=a5447816724d07ce93b1f5320367a550&units=imperial`).
    //       map((response: Response) => response.json()));
    //   }
    // }
    // return responses.map((response: Response) => response.json());
  }
  // localWeatherZip(zip:string){
  //   return this.http.get(`http://samples.openweathermap.org/data/2.5/weather?zip=${zip},us
  //   &appid=a5447816724d07ce93b1f5320367a550&units=imperial`).map((response:Response) => response.json())
  // }

  nextDaysForecast(zip: string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?zip=${zip}&appid=a5447816724d07ce93b1f5320367a550&units=imperial`).
      map((response: Response) => response.json())
  }

}
