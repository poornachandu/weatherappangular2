import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders, NgModule} from '@angular/core';
import { CurrentWeatherComponent } from './current-weather/current-weather.component';
import { ForecastComponent } from './forecast/forecast.component';
import { ResolvePromiseService } from './resolve-promise.service';

const WEATHER_ROUTER:Routes = [
    {path: '', component: CurrentWeatherComponent, resolve: {myWeather:ResolvePromiseService}},
    {path: 'current', component: CurrentWeatherComponent, resolve: {myWeather:ResolvePromiseService}},
    {path: 'forecast', component: ForecastComponent}
]

// @NgModule({
//     imports: [RouterModule.forRoot(WEATHER_ROUTER)],
//     exports: [RouterModule]
//     // exported coz, other components can make use of this
//   })
//   export class AppRoutingModule{}
//   export const weatherRouting = [CurrentWeatherComponent, ForecastComponent]

// OR

export const weatherRouting:ModuleWithProviders = RouterModule.forRoot(WEATHER_ROUTER);